# 2DXConvert
Tool to convert samples within .2dx files that worked in older LDJ engines to a sample rate supported by WASAPI implementations.

## Usage
Drop a **folder**, or multiple **folders** onto the script, or execute the script in command line as such:
```
python convert.py C:\path\to\folder\containing\2dx\files
```