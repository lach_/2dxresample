import os
import sys
import subprocess
import re
import shutil


# Terrible, slow, poorly written conversion script to unpack, resample, and repack
# files for the correct sample rate for WASAPI-reliant engines. Those fuckers only take
# 44.1kHz and maaaaaayyyybeeee 48kHz samples in MS-ADPCM format ONLY.

# Version 4.20 alpha

# This script is licensed under the WTFPL. A copy can be found at http://www.wtfpl.net/
# I am not responsible for any broken installs, data loss due to misconfiguration, erased
# system32, root directories, or otherwise. In fact, you probably should just redownload the
# data and save yourself the headache of dealing with this.
#
# To speed things up dramatically, run the script in blocks of folders, because the encoder
# takes its sweet fucking time and I don't know how multiprocessing works. Consider just not
# messing with this shit.


# Modify these to match your root file path. You'll have to get a copy of Sox binary for your OS,
# as well as copies of 2dxDump/2dxBuild by mon. Both of which can be found here:
# https://github.com/mon/2dxTools
dump_exe = "C:/Users/lach/Desktop/resampling/2dxDump.exe"
build_exe = "C:/Users/lach/Desktop/resampling/2dxBuild.exe"
sox = "C:/Users/lach/Desktop/resampling/sox.exe"


# finds the song data .2dx container, and not the preview one.
def find_file(path):
    for file in os.listdir(path):
        if(re.search("\d{5}\.2dx", file)):
            return file


# Creates a list of .wav files and passes them back. Only used in resampling and deletion.
def find_samples(path):
    samples = []
    for file in os.listdir(path):
        if(re.search("\d\.wav", file)):
            samples.append(file)
    return samples


# I wronte this one first, which is why it uses weird arguments compared to everything else.
# Don't feel like changing it. Runs 2dxDump.exe with the 2dx container as arg.
def unpack(fname):
    filename = find_file(fname)
    args = dump_exe + ' "' + fname + filename + '"'
    print(args)
    subprocess.call(args)


# Calls the sample function and runs it through sox to resample 44.1kHz. Some issues with
# file locking. Sox can't seem to overwrite itself for whatever reason even though the original
# samples are useless from this point onwards, so we create a temp folder.
def convert(folder):
    samples = find_samples(folder)
    if os.path.exists('output') is False:
        os.mkdir('output')
    print('We\'re just going to wait here for a bit while we encode the above samples')
    for x in samples:
        args = sox + ' -G ' + x + ' -e ms-adpcm -r 44100 output/' + x
        subprocess.call(args)


# Repackages the samples back into the .2dx container. Can probably do this without changing
# working directory, but 2dxBuild and Dump use their working directory as the target.
def package(folder):
    filename = find_file(folder)
    # print(filename)
    os.chdir(r'output')
    # print(os.getcwd())
    # print(folder)
    args = build_exe + ' ../' + folder[-6:-1] + '.2dx'
    subprocess.call(args)
    os.chdir('..')


# Cleans up the song folder. This is where something would go really wrong, if it was going to.
def cleanup(folder):
    print('Cleaning up directories...')
    samples = find_samples(folder)
    print(samples)
    for x in samples:
        os.remove(x)
    if os.path.exists('output') is True:
        shutil.rmtree('output')


# we set this to 1 because the script call is argument 0, weird but okay.
x = 1

# Good god what did i do here. I think in my head I intended to not have functions call other
# functions for whatever reason. This was meant to be quick and dirty so it's no big deal.
while x < len(sys.argv):
    dir = sys.argv[x] + "\\"
    path = os.path.dirname(dir)
    os.chdir(path)
    unpack(dir)
    convert(dir)
    package(dir)
    cleanup(dir)
    x += 1  # increment counter to make sure we actually go somewhere
